#!/usr/bin/env python3
"""
Daemon to convert AirPort syslog messages to Home Assistant personal presence.
HA will create a device tracker for each MAC address it receives.
It will also create a binary sensor for the Internet connection being live.

Set your AirPort device to send syslog messages to the IP of your Home Assistant.
You'll need an old Windows tool for this (Mac version doesn't give access to SysLog).
https://support.apple.com/kb/DL1547?viewlocale=en_US&locale=en_US
  or
https://www.techspot.com/downloads/5242-apple-airport-utility-for-windows.html
"""

import os
import sys
import socket
import subprocess
import json
import yaml
from requests import post

HOST_NAMES = {}
TOKEN = os.getenv('SUPERVISOR_TOKEN')
HEADERS = {'Authorization': f'Bearer {TOKEN}', \
           'content-type': 'application/json'}
SENSOR_CMD = 'http://supervisor/core/api/states/binary_sensor.Internet'
DEVICE_CMD = 'http://supervisor/core/api/config/services/device_tracker/see'

ADDRESS = ('0.0.0.0', 514)
TIMEOUT = "180" #sec

def main():
    """ Main thing """
    # Setup listening port
    try:
        sckt = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        sckt.bind(ADDRESS)
    # pylint: disable=bare-except
    except:
        print("Error binding to port", flush=True)
        sys.exit(1)
    # pylint: enable=bare-except
    print('Airconn started', flush=True)

    # Test current internet status and report
    if subprocess.call(['ping', '-c', '4', 'google.com']) == 0:
        rmes = 'pppoe: Connection established'
    else:
        rmes = 'pppoe: Disconnected.'

    # Assume all devices are home
    for mac, host in HOST_NAMES.items():
        post_data = '{"mac":"' + mac + '","source_type":"router",' + \
                    '"host_name":"' + host + '","consider_home":"99:00:00"}'
        print(f'Assume {host} is home', flush=True)
        post(DEVICE_CMD, data=post_data, headers=HEADERS)


    try:
        while True:
            try:
                if not rmes:
                    # No initial message, wait for one
                    rdata = sckt.recvfrom(8092)[0]
                    rmes = str(rdata)

                if 'pppoe: Disconnected.' in rmes or 'ether: (WAN) link state is Down' in rmes:
                    post_data = '{"state":"off", "attributes": {"wan_ip":""}}'
                    print('Reporting Internet down', flush=True)
                    post(SENSOR_CMD, data=post_data, headers=HEADERS)

                elif 'pppoe: Connection established' in rmes:
                    wan_ip = rmes[rmes.find('established')+12:rmes.find('->')-1]
                    post_data = '{"state":"on", "attributes": {"wan_ip": "'+wan_ip+'"}}'
                    print('Reporting Internet up', flush=True)
                    post(SENSOR_CMD,data=post_data, headers=HEADERS)

                elif 'Disassociated with station' in rmes:
                    mac = rmes[-18:-1]
                    if mac in HOST_NAMES:
                        name = HOST_NAMES[mac]
                        print(f'Report absence of {name}', flush=True)
                        post_data = '{"mac":"' + mac + '","source_type": "router",' + \
                            '"host_name":"' + name + '", \
                            "consider_home":"' + TIMEOUT + '"}'
                        post(DEVICE_CMD, data=post_data, headers=HEADERS)

                elif 'Installed unicast CCMP key for supplicant' in rmes:
                    mac = rmes[-18:-1]
                    if mac in HOST_NAMES:
                        name = HOST_NAMES[mac]
                        print(f'Report presence of {name}', flush=True)
                        post_data = '{"mac":"' + mac + '","source_type":"router",' + \
                            '"host_name":"' + name + '","consider_home":"99:00:00"}'
                        post(DEVICE_CMD, data=post_data, headers=HEADERS)
                rmes = ''
            except socket.error:
                pass
    except KeyboardInterrupt:
        print('Goodbye')
        sys.exit(0)


def read_cfg_file(cfg_option: str, data_path: str) -> dict:
    """" Read json or yaml file and return as dict """

    if cfg_option and cfg_option.startswith('/'):
        name = cfg_option
    elif cfg_option:
        name = os.path.join(data_path, cfg_option)
    else:
        name = os.path.join(data_path, 'configuration.json')
        if not os.path.exists(name):
            name = os.path.join(data_path, 'configuration.yaml')

    data = {}
    if name and os.path.exists(name):
        print(f'Reading file {name} as json', flush=True)
        with open(name, 'r', encoding='utf8') as yfile:
            try:
                data = json.load(yfile)
            except json.JSONDecodeError:
                pass
    if not data:
        with open(name, 'r', encoding='utf8') as yfile:
            print(f'Reading file {name} as yaml', flush=True)
            try:
                data = yaml.safe_load(yfile)
            except yaml.scanner.ScannerError:
                print(f'Incorrect YAML or JSON file {name}', flush=True)
                sys.exit(1)
    return data


def get_hostnames(hosts: list) -> dict:
    """ Return dict of hostnames """
    lookup = {}
    for item in hosts:
        mac = item.get('mac')
        name = item.get('name')
        if mac and name:
            lookup[mac] = name
            print(f'Found mac={mac} name={name}', flush=True)
    return lookup


if __name__ == '__main__':
    config = read_cfg_file(None, os.getenv('AIRCONN_DATA', '.'))
    HOST_NAMES = get_hostnames(config.get('hostnames', ()))
    sys.exit(main())
