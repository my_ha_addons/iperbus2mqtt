#!/usr/bin/env python3
"""
  IperBus to MQTT controller.
  (C) 2022-2023 by Cornelis Vissenberg

  Control the Iperbus without the vendor's controller.
  Connect your Iperbus USB cable to your system first.

  The software has two operating modes.
  Add-on for Home Assistant:
    Make sure the environment variable IPERBUS2MQTT_DATA points to
    the folder containing configuration.yaml or configuration.json.
    No command-line options are needed.
  Stand-alone:
    Use --config or --data to specify the configuration file.
    You will also have extra command-line options available.
"""
import sys
import os
import signal
import argparse
import re
import time
import logging
from logging.handlers import RotatingFileHandler
from binascii import unhexlify, hexlify
import json
import asyncio
from contextlib import AsyncExitStack

import serial
import yaml

from paho.mqtt.client import MQTTv5
from aiomqtt import Client, Will, MqttError


# Requires pip packages:
# - pyserial
# - pyyaml
# - paho-mqtt
# - aiomqtt

#   pylint: disable=too-few-public-methods,line-too-long

# TODO: Rewrite serial stuff with serial_asyncio streams:
#       https://tinkering.xyz/async-serial/


#################################################################
# Default config values

DEF_IPERBUS =     '/dev/serial/by-id/usb-Urmet_1071_56_Urmet_IPerBus_Interface-if00'
DEF_IPERBUS_ALT = '/dev/serial/by-id/usb-FTDI_USB-RS232_Cable_FTXIDK21-if00-port0'
DEF_SERVER  = 'mqtt://homeassistant.local:1883'
DEF_LOGGING = 'INFO'
DEF_TOPIC   = 'iperbus2mqtt'
CONFIG_ENV  = 'IPERBUS2MQTT_DATA'
MAX_RETRIES = 5

#################################################################
# Tables and Constants

LOG_LEVELS = ('DEBUG', 'INFO', 'WARN', 'WARNING', 'ERROR')

# Topics used in MQTT messages
CONFIG_TOPIC = 'homeassistant'
DEVICE_TOPIC = 'iperbus'

# Device classes mapped to MQTT
CLASS_TYPES = {'outlet': 'switch',
               'switch': 'switch',
               'light':  'light',
               'fan':    'fan',
               'button': 'button',
              }
CLASS_ICONS = { 'outlet': 'mdi:power-socket-de',
              }


# Define Iperbus device types
PREFIX = 'iperbus_' # Prefix for every device name
PREFIX_R = 'R'      # Single relay channel
PREFIX_I = 'I'      # Single input line
PREFIX_T = 'T'      # Thermostat sensor
PREFIX_X = 'X'      # Thermostat programmer
PREFIX_V = 'V'      # Single virtual input line (non-physical)

# Translation of device types to Urmet models
MODELS = {
    PREFIX_R: '1071/14', # 4 Line Relay
    PREFIX_I: '1071/18', # 8 Line Input
    PREFIX_T: '1071/32', # Thermostat sensor
    PREFIX_X: '1071/31', # Thermostat programmer
}

# Define known Iperbus command codes
IB_EVENT =  b'\xf8'     # Device announces an event
IB_STATUS = b'\xfb'     # Device reports its current status
IB_EVENT_INT = IB_EVENT[0]
IB_STATUS_INT = IB_STATUS[0]
IPER_CMD = {
    IB_EVENT_INT: 'EVENT',
    IB_STATUS_INT: 'STATUS',
}

# Channel codes of input device for "on"
# These come from the 4-line relays
CHANNELS_ON = [
    unhexlify('00010000'), # CH-1-on
    unhexlify('00020000'), # CH-2-on
    unhexlify('00040000'), # CH-3-on
    unhexlify('00080000'), # CH-4-on
    unhexlify('00100000'), # CH-5-on
    unhexlify('00200000'), # CH-6-on
    unhexlify('00400000'), # CH-7-on
    unhexlify('00800000'), # CH-8-on
]
# Channel codes of input device for "off"
CHANNELS_OFF = [
    unhexlify('00000100'), # CH-1-off
    unhexlify('00000200'), # CH-2-off
    unhexlify('00000400'), # CH-3-off
    unhexlify('00000800'), # CH-4-off
    unhexlify('00001000'), # CH-5-off
    unhexlify('00002000'), # CH-6-off
    unhexlify('00004000'), # CH-7-off
    unhexlify('00008000'), # CH-8-off
]

# Response codes from 4-line relays.
# The purpose is not clear.
# TODO: add CH-1, 2 and 3
INPUT_ON = [
    unhexlify('F608'), # CH-4-off
]

INPUT_OFF = [
    unhexlify('F508'), # CH-4-off
]


class Terminator:
    """ Global list of class-instances that require a stop signal."""
    instances : list = []


def is_true(value) ->  bool:
    """ Return True if it looks like True """
    return str(value).lower() in ('y', 'yes', 'true', '1', 'on')


def fatal(message: str) -> None:
    """ Log message and exit with 1."""
    print(message, flush=True)
    logging.fatal(message)
    sys.exit(1)


class PatchedClient(Client):
    """ Client with an extra class-attribute."""
    # Just adding iper_topic at run-time will upset MyPy.
    iper_topic : str = ''


#################################################################
# Iperbus Section

class IperBus:
    """Client to connect with IperBus."""
    # pylint: disable=too-many-instance-attributes

    # self._all_devices    : Devices {<address>: <Type-letter>}
    # self._active_devices : Active devices and their state
    #                           { 'iperbus_R2_2': {
    #                               'name' : 'Slaapkamer wandlamp',
    #                               'class': 'light',
    #                               'area' : 'Slaapkamer',
    #                               'model': '1071/14',
    #                               'icon' : None | icon-name
    #                               'state': True,
    #                             }
    #                           }
    # self._virtual_map    : Map relays to virtual inputs
    #                           {'iperbus_R1_2' : 'V12_2'}
    # self._virtuals       : List of virtual input devices addresses
    #                           [ 12, 13 ]
    # self._statequeue     : List of states to be confirmed by relay status-message
    #                           { 'iperbus_R1_2': (<timestamp>, <state>, <count>) }

    instance = None     # Running instance, used by sig_handler
    stop = False        # Stop signal, set by sig_handler
    re_address = re.compile(r'^(\d+)/(\d+)$')
    last_states = ''    # Last saved json content

    def __init__(self, config: dict, datapath: str) -> None:
        """ Setup Iperbus connection. """
        # Set status structure for the relays
        self._debug = logging.DEBUG >= logging.root.level
        self._all_devices : dict = {}
        self._active_devices : dict = {}
        self._log_temp = config.get('log_temp', False)
        self._virtual_map : dict = {}
        self._datapath = datapath
        self.statequeue : dict = {}

        layout = config.get('layout', ())
        self.analyse_layout(layout)
        self.virtual_table(layout)
        self.restore_status()

        port = config.get('iperbus', DEF_IPERBUS)
        if port == '-':
            # Test mode without iperbus connection
            self._handle = None
        else:
            self._handle = serial.Serial(port, 19200, timeout=0.1)


    def create_real_device(self, device: dict, devname: str):
        """ Create a real device record."""
        match = self.re_address.search(str(device[devname]))
        if match:
            address = match.group(1)
            line = match.group(2)
        else:
            address = device[devname]
            line = 0
        unit = devname[0].upper()
        self._all_devices[int(address)] = unit
        name = device.get('name')
        devclass = CLASS_TYPES.get(device.get('class', ''), 'switch')
        area = device.get('area', '')
        model = MODELS.get(unit, 'unknown')
        if name and devclass:
            dev_id = f'{PREFIX}{unit}{address}'
            if line:
                dev_id += f'_{line}'
            self._active_devices[dev_id] = {
                'name':  name,
                'class': devclass,
                'area':  area,
                'model': model,
                'icon' : CLASS_ICONS.get(device.get('class', '')),
                'state': False,
                }


    def analyse_layout(self, layout: list) -> None:
        """ Setup layout tables. """
        for device in layout:
            if isinstance(device, dict):
                if 'relay' in device:
                    self.create_real_device(device, 'relay')
                elif 'input' in device:
                    self.create_real_device(device, 'input')
                elif 'thermostat' in device:
                    self.create_real_device(device, 'thermostat')
                elif 'xthermostat' in device:
                    self.create_real_device(device, 'xthermostat')
                elif 'virtual' in device:
                    continue
                else:
                    logging.warning('Undefined iperbus device %s', device)


    def virtual_table(self, layout: dict) -> None:
        """ Create a mapping from virtual input unit to relay lines."""
        for device in layout:
            if isinstance(device, dict) and 'virtual' in device:
                address = device.get('virtual')
                relay_address = device.get('map', '')
                relay = f'{PREFIX}{PREFIX_R}{relay_address}_'
                for line in range(5):
                    # 'iperbus_R1_2': 'V12.2'
                    relay_line = f'{relay}{line+1}'
                    if address and relay_line in self._active_devices:
                        self._virtual_map[relay_line] = f'V{address}_{line+1}'


    def close(self) -> None:
        """ Close Iperbus connection """
        if self._handle:
            self._handle.close()
            self._handle = None


    def restore_status(self) -> None:
        """ Restore device statuses from file.
            Expected format:
            { 'iper-id-1' : False,
              'iper-id-2' : True
            }
        """
        save_path = os.path.join(self._datapath, 'state.json')
        logging.debug('Restoring state from file "%s"', save_path)
        try:
            with open(save_path, 'r', encoding='utf8') as fhandle:
                states = json.load(fhandle)
        except (IOError, json.JSONDecodeError):
            logging.warning('No valid state file found at "%s"', save_path)
            return
        if not isinstance(states, dict):
            logging.warning('No valid state file found at "%s"', save_path)
            return
        for dev, state in states.items():
            if dev in self._active_devices:
                self._active_devices[dev]['state'] = bool(state)


    def save_status(self) -> None:
        """ Save device statuses to a file.
            Format:
            { 'iper-id-1' : False,
              'iper-id-2' : True
            }
        """
        try:
            states = json.dumps({x: y['state'] for x, y in self._active_devices.items()})
        except json.JSONDecodeError:
            logging.error('Cannot convert states to JSON')
            return
        if states != self.last_states:
            save_path = os.path.join(self._datapath, 'state.json')
            logging.debug('Saving state to file "%s"', save_path)
            try:
                with open(save_path, 'w', encoding='utf8') as fhandle:
                    fhandle.write(states)
                self.last_states = states
            except IOError:
                logging.error('Cannot save to "%s"', save_path)


    async def get_message(self) -> tuple:
        """ Receive one IperBus message """
        # <0F> <command> <unit> <size> <load> <checksum> <04>

        assert self._handle is not None
        cmd, address, load = (0, 0, b'')
        while not self.stop:
            # Try to get <0F><command><unit><size>
            header = self._handle.read(4)
            while header and header[0] != 0x0F:
                header = header[1:]
            if not header:
                await asyncio.sleep(0)
                continue
            if len(header) < 4:
                # Incomplete header, try to read more
                header = header + self._handle.read(4-len(header))
            if len(header) < 4:
                logging.warning('Iperbus: malformed message (header): %s', hexlify(header))
                continue
            cmd = header[1]
            address = header[2]
            size = header[3]

            # Read the rest of the message
            tail = self._handle.read(size+2)
            load = tail[:-2]
            checksum_recv = tail[-2]

            # Verify message
            checksum = 0
            for item in header + load:
                checksum = (checksum + item) % 256
            checksum = (checksum ^ 255) + 2
            if checksum != checksum_recv or tail[-1] != 0x04:
                logging.warning('Iperbus: malformed message (checksum): %s', hexlify(header+load))
                continue
            break
        return cmd, address, load


    def log_ipercmd(self, cmd: int, address: int, load: bytes) -> None:
        """ Log an iperbus message."""
        if self._log_temp or self._all_devices.get(address, '') not in ('',PREFIX_T,PREFIX_V, PREFIX_X):
            cmd_str = IPER_CMD.get(cmd, cmd)
            if load in CHANNELS_OFF:
                load_str = f'Line-{CHANNELS_OFF.index(load)+1} off'
            elif load in CHANNELS_ON:
                load_str = f'Line-{CHANNELS_ON.index(load)+1} on'
            else:
                load_str = hexlify(load).decode()
            logging.debug('Iperbus: received %s %s %s', cmd_str, address, load_str)


    async def process_messages(self, broker: PatchedClient | None) -> None:
        """ Read and process Iperbus messages. """
        # pylint: disable=too-many-branches
        logging.info('Iperbus: starting receive process')
        while self._handle and not self.stop:
            cmd, address, load = await self.get_message()
            if self._debug:
                self.log_ipercmd(cmd, address, load)
            if cmd != IB_EVENT_INT:
                continue
            if address in self._all_devices:
                state = True
                if self._all_devices[address] in (PREFIX_I, PREFIX_R):
                    if load in CHANNELS_ON:
                        line = CHANNELS_ON.index(load)
                    elif load in CHANNELS_OFF:
                        line = CHANNELS_OFF.index(load)
                        state = False
                    else:
                        logging.warning('Unknown line state in event message')
                        continue
                    idd = f'{PREFIX}{self._all_devices.get(address)}{str(address)}_{line+1}'
                    device = self._active_devices.get(idd)
                    if device:
                        device['state'] = state
                        devclass = device['class']
                        if idd in self.statequeue:
                            # If a timer was active, remove it.
                            logging.debug('Remove state-timer for %s:%s', idd, state)
                            self.statequeue.pop(idd)
                        await send_state(broker, idd, devclass, state)
                elif self._all_devices[address] in ('T', 'X'):
                    continue
                else:
                    logging.warning('Iperbus: Unknown event %s event %s', \
                                     address, hexlify(load))
                    continue
            elif broker:
                # Without broker, we're in monitor mode. Then ignore unknown devices (could be virtual).
                logging.debug('Iperbus: ignore inactive iperbus device with address %s"', address)

        logging.info('Iperbus: ending receive process')
        if broker:
            await broker.disconnect()
        self.close()



    def send_message(self, cmd: bytes, address: int, data: bytes) -> bool:
        """Send Iperbus message. """
        size = len(data)
        msg = b'\x0f' + cmd + address.to_bytes(1, 'big') + size.to_bytes(1, 'big') + data
        checksum = 0
        for item in msg:
            checksum = (checksum + item) % 256
        checksum = (checksum ^ 255) + 2
        msg =  msg + checksum.to_bytes(1, 'big') + b'\x04\n'
        if self._handle:
            self._handle.write(msg)
        if self._debug:
            # Extra check to avoid expensive parameters
            logging.debug('Iperbus: send %s %s %s', IPER_CMD.get(cmd[0], cmd[0]), address, hexlify(data))
        return True


    async def set_relay(self, relay: str, state: bool) -> bool:
        """Turn on relay line (eg iperbus_R1_2)."""
        # In reality it is sent to the virtual input
        vdevice = self._virtual_map.get(relay)
        if vdevice:
            # 'V1_2' --> ('V1', '2')
            try:
                address, line = vdevice.split('_')
                address = int(address[1:])  # 'V1' --> 1
                line = int(line) - 1        # '2'  --> 2
                data = CHANNELS_ON[line] if state else CHANNELS_OFF[line]
            except (IndexError, KeyError, ValueError):
                logging.error('MQTT: using invalid relay device %s', relay)
                vdevice = None
        if vdevice:
            return self.send_message(IB_EVENT, address, data)
        return False


    async def press_button(self, button: str) -> bool:
        """ Send a button press """
        _, address, line = button.split('_')
        iaddress = int(address[1:]) # 'I1' --> 1
        iline = int(line) - 1          # '2'  --> 2
        data = CHANNELS_ON[iline]
        return self.send_message(IB_EVENT, iaddress, data)


    def status(self, relay: str) -> bool:
        """Return status of relay switch (eg iperbus_R1_2)."""
        try:
            return self._active_devices[relay]['state']
        except (IndexError, KeyError):
            return False


    async def console_handler(self) -> None:
        """ Read commands from console: <unit>.<line> on|off"""
        while not self.stop:
            loop = asyncio.get_event_loop()
            line = await loop.run_in_executor(None, sys.stdin.readline)
            assert isinstance(line, str)
            line = line.strip()
            logging.info('Iperbus: read command line "%s"', line)
            if line == 'stop':
                self.stop = True
                break
            if not line:
                continue
            unit, state = line.split()
            unit = unit if unit.startswith(PREFIX) else f'{PREFIX}{unit}'
            await self.set_relay(unit, state=='on')


    def get_device_names(self) -> list:
        """Return device names as a list of dicts."""
        devices = []
        for idd, value in self._active_devices.items():
            devices.append({'id': idd, 'name': value['name'], 'class': value['class'],
                            'model': value['model'], 'area': value['area'], 'icon': value['icon']})
        return devices


    async def send_dev_status(self, broker, device: str) -> None:
        """ Send current devices statuses """
        await send_state(broker, device, self._active_devices[device]['class'], \
                             self._active_devices[device]['state'])


    async def send_dev_statuses(self, broker) -> None:
        """ Send current devices statuses """
        for idd, value in self._active_devices.items():
            await send_state(broker, idd, value['class'], value['state'])



def sig_handler(signum=None, frame=None) -> bool:
    """ Console break handler."""
    # pylint: disable=unused-argument
    if os.name == 'nt' and signum is not None and signum == 5:
        # Ignore the "logoff" event when running as a Win32 daemon
        return True
    if signum is not None:
        print('Stop request Iperbus2mqtt', flush=True)
        logging.warning('Signal %s caught, saving and exiting...', signum)
        for instance in Terminator.instances:
            if hasattr(instance, 'save_status'):
                instance.save_status()
            if hasattr(instance, 'stop'):
                instance.stop = True
    os._exit(1)
    return True


async def iperbus_state_checker(iper: IperBus) -> None:
    """ Iperbus state checker task."""
    # Check devices for status confirmation.
    # If the time exceeds 1 second, the re-send the relay command.
    # pylint: disable=too-many-nested-blocks
    logging.info('Iperbus: start state checker process')
    while not iper.stop:
        await asyncio.sleep(0.5)
        if iper.statequeue:
            now = time.time()
            queue = list(iper.statequeue.keys())
            for device in queue:
                try:
                    temp = iper.statequeue[device]
                    stamp, state, count = temp
                except KeyError:
                    # Already removed by state confirmation
                    continue
                except ValueError:
                    # Should not happen
                    logging.warning('BAD STATEQUEUE ITEM %s = %s', device, temp)
                    stamp = now - 2.0
                    count = 0
                if stamp + 1.0 < now:
                    if count > 0:
                        logging.debug('Repeat state-timer for %s:%s', device, state)
                        iper.statequeue[device] = (time.time(), state, count-1)
                        await iper.set_relay(device, state)
                    else:
                        # Enough retries, the state probably wasn't changed.
                        # This can happen if no actual state change occured.
                        try:
                            iper.statequeue.pop(device)
                        except KeyError:
                            pass


async def save_timer(iper: IperBus) -> None:
    """ State save timer task."""
    logging.info('Iperbus: start save timer process')
    while not iper.stop:
        await asyncio.sleep(15*60)
        iper.save_status()



async def manual_input_task(iper: IperBus) -> None:
    """ Handle manual input """
    logging.info('Iperbus: start command handler')
    await iper.console_handler()



#################################################################
# MQTT Section

def setup_broker(config: dict) -> PatchedClient:
    """ Connection to mqtt-broker. """
    server = DEF_SERVER
    broker_user = None
    broker_password = None
    topic = DEF_TOPIC

    mqtt = config.get('mqtt')
    if mqtt:
        server = mqtt.get('server', DEF_SERVER)
        broker_user = mqtt.get('user')
        broker_password = mqtt.get('password')
        topic = mqtt.get('base_topic', DEF_TOPIC)

    match = re.search(r'^([^:]+):(\d+)', server.replace('mqtt://', ''))
    if match:
        broker_host = match.group(1)
        broker_port = int(match.group(2))
    else:
        fatal(f'Incorrect broker URL = {server}')

    lwt = Will(f'{topic}/bridge/state', payload='offline', retain=True)
    logging.info('MQTT: connect to broker at %s:%s', broker_host, broker_port)
    broker = PatchedClient(
                broker_host,
                broker_port,
                username=broker_user,
                password=broker_password,
                client_id='iperbus',
                protocol=MQTTv5, # type: ignore
                will=lwt,
                clean_start=True)
    broker.iper_topic = topic
    return broker



async def send_state(broker: PatchedClient|None, unique_id: str, devclass: str, state: bool) -> None:
    """ Send a state message to the mqtt-broker """
    if broker:
        topic = f'{broker.iper_topic}/{devclass}/{unique_id}/{DEVICE_TOPIC}/state'
        payload = b'ON' if state else b'OFF'
        logging.debug('MQTT: send %s = %s', topic, payload)
        await broker.publish(topic, payload)



async def broker_message_handler(iper: IperBus, broker: PatchedClient, messages) -> None:
    """ Incoming broker message handler. """
    async for message in messages:
        topic = message.topic
        payload_bytes = message.payload
        logging.debug('MQTT: received %s = %s', topic, payload_bytes)
        if topic.startswith('homeassistant/status'):
            if payload_bytes and b'online' in payload_bytes:
                # When Home Assistant says it's online, resend device list
                await send_config(broker, iper)
                await iper.send_dev_statuses(broker)
            continue
        try:
            _, devclass, unique_id, _, cmd = topic.split('/')
        except ValueError:
            logging.warning('MQTT: invalid %s = %s', topic, payload_bytes)
        try:
            payload = json.loads(payload_bytes)
        except (ValueError, json.decoder.JSONDecodeError):
            payload = {'state': payload_bytes.decode('utf8', errors="replace").strip().lower()}
        state = payload.get('state', '').lower() in ('on', 'press')
        if cmd == 'set':
            if devclass in ('switch', 'light', 'fan'):
                # Add to timer queue for status checking
                logging.debug('Add state-timer for %s:%s', unique_id, state)
                iper.statequeue[unique_id] = (time.time(), state, MAX_RETRIES)
                # Send command to relay
                await iper.set_relay(unique_id, state)
                # Force state update for broker, because relay will not respond
                # if the state doesn't change!
                await send_state(broker, unique_id, devclass, state)
            elif devclass == 'button' and state:
                await iper.press_button(unique_id)
            else:
                logging.info('MQTT: ignoring: %s = %s', topic, payload)
        else:
            logging.info('MQTT: ignoring: %s = %s', topic, payload)



async def send_config(broker: PatchedClient, iper: IperBus) -> None:
    """ Translate iperbus layout to MQTT device definitions """
    items = iper.get_device_names()
    for item in items:
        name = item['name']
        unique_id = item['id']
        match = re.search(r'(\d+)_(\d+)', unique_id)
        if match:
            line = f'#{match.group(1)}, Line {match.group(2)}'
        else:
            line = unique_id
        devclass = item['class']
        topic = f'{CONFIG_TOPIC}/{devclass}/{unique_id}/{DEVICE_TOPIC}/config'
        payload = { 'avty':       [{'topic': f'{broker.iper_topic}/bridge/state'}],
                    '~':          f'{broker.iper_topic}/{devclass}/{unique_id}/{DEVICE_TOPIC}',
                    'name':       name,
                    'unique_id':  unique_id,
                    'cmd_t':      '~/set',
                    'stat_t':     '~/state',
                    'device': {
                        'ids':    [unique_id],
                        'mf':     'Urmet',
                        'name':   name,
                        'sa':     item['area'],
                        'model':  f'{item["model"]} {line}',
                        'sw':     '1.0'
                        },
                    }
        if item['icon']:
            payload['icon'] = item['icon']
        if devclass == 'light':
            payload['brightness'] = False
        logging.debug('MQTT: send %s = %s', topic, payload)
        payload_json = json.dumps(payload)
        await broker.publish(topic, payload_json)
        await iper.send_dev_status(broker, unique_id)
    await broker.publish(f'{broker.iper_topic}/bridge/state', 'online')



async def cancel_tasks(tasks: set) -> None:
    """ Cancel running tasks """
    for task in tasks:
        if task.done():
            continue
        try:
            task.cancel()
            await task
        except asyncio.CancelledError:
            pass


async def task_building(config: dict, datapath: str, monitor=False) -> None:
    """ Setup tasks and start. """
    iper = IperBus(config, datapath)
    Terminator.instances.append(iper)
    first = True

    while not iper.stop:
        try:
            async with AsyncExitStack() as stack:
                tasks: set = set()
                stack.push_async_callback(cancel_tasks, tasks)

                if not monitor:
                    broker = setup_broker(config)
                    await stack.enter_async_context(broker)

                    topics = (f'{broker.iper_topic}/+/+/{DEVICE_TOPIC}/set',
                              'homeassistant/status' )
                    for topic in topics:
                        manager = broker.filtered_messages(topic)
                        messages = await stack.enter_async_context(manager)
                        tasks.add(asyncio.create_task(broker_message_handler(iper, broker, messages)))

                    for topic in topics:
                        await broker.subscribe(topic)

                    if first:
                        # Only send full config at startup, not for reconnects.
                        tasks.add(asyncio.create_task(send_config(broker, iper)))
                        first = False
                    tasks.add(asyncio.create_task(iper.process_messages(broker)))
                else:
                    tasks.add(asyncio.create_task(iper.process_messages(None)))
                    tasks.add(asyncio.create_task(manual_input_task(iper)))

                tasks.add(asyncio.create_task(iperbus_state_checker(iper)))
                tasks.add(asyncio.create_task(save_timer(iper)))

                await asyncio.gather(*tasks)
        except MqttError as error:
            logging.info('MQTT: lost communication (%s), reconnecting', error)



def read_cfg_file(cfg_option: str, data_path: str) -> dict:
    """" Read json or yaml file and return as dict """

    if cfg_option and cfg_option.startswith('/'):
        name = cfg_option
    elif cfg_option:
        name = os.path.join(data_path, cfg_option)
    else:
        name = os.path.join(data_path, 'configuration.json')
        if not os.path.exists(name):
            name = os.path.join(data_path, 'configuration.yaml')

    data = {}
    if name and os.path.exists(name):
        print(f'Reading file {name} as json', flush=True)
        with open(name, 'r', encoding='utf8') as yfile:
            try:
                data = json.load(yfile)
            except json.JSONDecodeError:
                pass
    if not data:
        with open(name, 'r', encoding='utf8') as yfile:
            print(f'Reading file {name} as yaml', flush=True)
            try:
                data = yaml.safe_load(yfile)
            except yaml.scanner.ScannerError:
                logging.fatal('Incorrect YAML or JSON file %s', name)
                sys.exit(1)
    return data


def setup_logging(config: dict, data_path: str, monitor=False) -> None:
    """ Setup logging to rotating files
    """
    loglevel = config.get('log_level', '').upper()
    loglevel = loglevel if loglevel in LOG_LEVELS else DEF_LOGGING
    logging.raiseExceptions = False

    logger = logging.getLogger()
    logger.setLevel(getattr(logging, loglevel))

    # Remove any existing logger
    logger.handlers = []
    # Start with console logging (not for --monitor option)
    if not monitor:
        logger.addHandler(logging.StreamHandler())

    if config.get('log_to_disk') or monitor:
        # Log to rotating set of 5 log files, with max size of 1M
        try:
            logdir = os.path.join(data_path, 'log')
            logfile = os.path.join(logdir, 'iperbus2mqtt.log')
            os.makedirs(logdir, exist_ok=True)
            log_handler = RotatingFileHandler(logfile, 'a+', 1024*1024, 5)
            logger.addHandler(log_handler)
        except IOError:
            logging.error('Cannot create logfile %s', logfile)

    # Set useful diagnostic formatting
    logformat = "%(asctime)s::%(levelname)s::[%(module)s:%(lineno)d] %(message)s"
    for handler in logger.handlers:
        handler.setFormatter(logging.Formatter(logformat))



def starter(config: dict, datapath: str, monitor: bool) -> None:
    """ Set break-handler and start working """

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    if os.name == 'nt':
        # MyPy ignore needed, else it will complain on Linux
        asyncio.set_event_loop_policy(asyncio.ProactorEventLoop()) # type: ignore
    asyncio.run(task_building(config, datapath, monitor))



def main():
    """ Main thing. """

    # Handle command line
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    # Options to overrule Home Assistant setup
    parser.add_argument('--data', dest='data_path',
                        help=f'Path to data files (defaults to environment variable {CONFIG_ENV}).')
    parser.add_argument('--config', dest='config',
                        help='Path to configuration file.')

    # Special diagnostic option
    parser.add_argument('--monitor', dest='monitor', action='store_true',
                        help='Monitor the iperbus and allow manual input, no MQTT connection. ' \
                             'Disables console logging and enables disk logging.')

    args = parser.parse_args()

    print('Starting Iperbus2mqtt', flush=True)

    # Determine data_path from environment or parameter
    data_path = args.data_path or os.getenv(CONFIG_ENV) or '.'
    data_path = os.path.abspath(data_path)

    # Analyse configuration file
    config = read_cfg_file(args.config, data_path)

    # Setup logging
    setup_logging(config, data_path, args.monitor)

    # Go, go, go
    starter(config, data_path, args.monitor)

    logging.info('Stopped Iperbus2mqtt')


if __name__ == '__main__':
    # pylint: disable=bare-except
    try:
        main()
    except:
        # Make sure we log any crash reason
        logging.exception('CRASHED!', exc_info=True)
