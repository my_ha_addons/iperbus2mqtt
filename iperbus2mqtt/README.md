# Home Assistant Add-on: Iperbus2mqtt

Allows you to use your Iperbus devices without the vendor's overpriced controller.

It translates IperBus devices to MQTT devices.
In this way you can integrate your Iperbus devices with whatever smart home infrastructure you are using.
Optimized for Home Assistant.

See Documentation tab for more details.
