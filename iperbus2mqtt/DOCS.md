# Configuration
## Data path
The default path for the persistent data files is `/config/iperbus2mqtt`.  
It can be overridden with the `data_path` parameter; the `/config` part is fixed by Home Assistant.


## Communication section
When used inside Home Assistant, you don't need any MQTT parameters.  
If you use an external MQTT broker, add the server, username and password.  
Example:
```
mqtt:
  server: 'mqtt://homeassistant.local:1883'
  user: iperbus_user
  password: go_iperbus_go
  base_topic: iperbus2mqtt
```

The iperbus requires a 1071/56 serial communication device or an RS232C to USB device.
```
 # 1071/56
iperbus: /dev/serial/by-id/usb-Urmet_1071_56_Urmet_IPerBus_Interface-if00
 # RS232C to USB
iperbus: /dev/serial/by-id/usb-FTDI_USB-RS232_Cable_FTXIDK21-if00-port0
```

## Logging
You can set logging:
```
log_level: debug
log_temp: false
log_to_disk: true

```

## Layout section
Describe the function of each element.  

### Relay
The relay needs address/line, where `address` is the iperbus address of the relay and `line` is the line number. `name` is the readable name of the connected device. `class` is `light`, `outlet` or `switch`. The `area` is the area name of your choice, make sure it matches the terms you have used in Home Assistant.  
Example:
```
- relay: 1/1
  name: Bedroom spot
  class: light
  area: Bedroom
```

### Input
The input needs address/line, where `address` is the iperbus address of the relay and `line` is the line number. `name` is the readable name of the connected device. `class` is always `button`. The `area` is the area name of your choice, make sure it matches the terms you have used in Home Assistant.
```
- input: 10/1
  name: Bedroom spot button
  class: button
  area: Bedroom
```

### Virtual input
There's a special device, called **V**irtual. This maps a virtual input device to a relay. This is needed to directly control the specified relay. Make sure you have set up the required mapping with the IperSet software.  
This example maps a virtual input unit at address 12 to the relay at address 1.
```
- virtual: 12
  map: 1
```

### Room Thermostat
Requires address, functionality not yet implemented.  
Example:
```
- thermostat: 23
  area: Livingroom
```

### Thermostat controller
Requires address, functionality not yet implemented.
Example:
```
- xthermostat: 35
  area: Service
```

# Configuration backup

The add-on will create a backup of your configuration.yaml within your data path: `$DATA_PATH/configuration.json.bk`.
The backup of your configuration is created on add-on startup if no previous backup was found. 
