<!-- https://developers.home-assistant.io/docs/add-ons/presentation#keeping-a-changelog -->

## 1.0.5

- Fix a crash due to incorrect handling of tuple

## 1.0.4

- Try a proper reconnect when mqtt communication fails.

## 1.0.3

- Limit the number of message retries.

## 1.0.2

- Fix missing relay messages.

## 1.0.1

- Many error fixes

## 1.0.0

- Initial release
