![alt text][logo]
# Home Assistant Add-on: iperbus2mqtt
Run [iperbus2mqtt] as a Home Assistant Add-on

## Installation
Add the repository URL under **Supervisor → Add-on store → ⋮ → Manage add-on repositories**:

    https://gitlab.com/sh_49/iperbus2mqtt/hassio-iperbus2mqtt

The repository includes one add-on:

- **iperbus2mqtt** is a stable release that tracks the released versions of iperbus2mqtt.

For more information see [the documentation](https://gitlab.com/ha_sh49/iperbus2mqtt/blob/master/iperbus2mqtt/DOCS.md).

# Changelog
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

All notable changes to this project will be documented in the [CHANGELOG.md](iperbus2mqtt/CHANGELOG.md) file.

Version for releases is based on [iperbus2mqtt](https://gitlab.com/ha_sh49/iperbus2mqtt) format: `X.Y.Z`.

Any changes on the addon that do not require a new version of [iperbus2mqtt] will use the format: `X.Y.Z-A` where `X.Y.Z` is fixed on the iperbus2mqtt release version and `A` is related to the addon.

# Issues
If you find any issues with the add-on, please check the [issue tracker](https://gitlab.com/ha_sh49/iperbus2mqtt/issues) for similar issues before creating one.

Feel free to create a PR for fixes and enhancements. 

# Credits
- [con.vissenberg](https://gitlab.com/con.vissenberg)

[logo]: iperbus2mqtt/logo.png "iperbus2mqtt"
[iperbus2mqtt]: https://gitlab.com/ha_sh49/iperbus2mqtt
